FROM node:16-alpine3.14 as builder
WORKDIR  /ts-playground
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
EXPOSE 3000
CMD [ "npm", "run", "start" ]