import mongoose from 'mongoose';

export const connect = async () => {

    try {
        const host = process.env.MONGO_HOST ?? 'localhost';
        const port = process.env.MONGO_PORT ?? '27017';
        const dbName = process.env.MONGO_DB_NAME ?? 'blog';
    
        await mongoose.connect(
            `mongodb://${ host }:${ port }/${ dbName }`,
            {
                authSource: process.env.MONGO_AUTH_SOURCE ?? 'admin',
                auth: {
                    username: process.env.MONGO_USERNAME ?? 'admin',
                    password: process.env.MONGO_PASSWORD ?? 'admin'
                },
            }
        );
    } catch (err) {
        console.error(err);
    }

};

export const disconnect = () => {
    mongoose.disconnect();
};
