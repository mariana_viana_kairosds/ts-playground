import mongoose from 'mongoose';

const connectToDB = async () => {
    const auth = process.env.MONGO_AUTH_SOURCE ?? 'admin';
    const auth_user = process.env.MONGO_AUTH_USER ?? 'admin';
    const auth_pass = process.env.MONGO_AUTH_PASS ?? 'admin';
    const host = process.env.MONGO_HOST ?? 'mongodb';
    const port = process.env.MONGO_PORT ?? '27017';
    const db_name = process.env.MONGO_DB_NAME ?? 'blog';
    try {
        await mongoose.connect(`mongodb://${host}:${port}/${db_name}`, {
            authSource: auth,
            auth: {
                username: auth_user,
                password: auth_pass,
            },
        });
    } catch (err) {
        console.log(err);
    }
};

export { connectToDB };
