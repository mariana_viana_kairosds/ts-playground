import Container from 'typedi';
import { User, UserType } from '../../domain/model/entities/user.entities';
import { EmailVO } from '../../domain/model/vos/email.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { NickNameVo } from '../../domain/model/vos/nickname.vo';
import { PasswordVO } from '../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../domain/model/vos/role.vo';
import { UserService } from '../../domain/services/user.service';

const populate = async(): Promise<void> => {
    const useService = Container.get(UserService);
    const userData: UserType = {
        id: IdVO.create(),
        nickName: NickNameVo.create('admin'),
        email: EmailVO.create('admin@gmail.com'),
        password: PasswordVO.create('admin'),
        role: RoleVO.create(Role.ADMIN)
    };
    await useService.persist(new User(userData));
};

export { populate as populateDatabases };
