import { Sequelize } from 'sequelize';
import { populateDatabases } from './populate';

const user = process.env.PG_USER ?? 'pguser';
const pass = process.env.PG_PASS ?? 'pguser';
const host = process.env.PG_HOST ?? 'dbpostgres';
const port = process.env.PG_PORT ?? '5432';
const dbName = process.env.PG_DBNAME ?? 'pgdb';

const sequelize = new Sequelize(`postgres://${user}:${pass}@${host}:${port}/${dbName}`);

sequelize.authenticate().then(()=>{
    console.log('conection access');
}).catch((err)=> console.log(err));

sequelize.sync({force: true}).then(()=> console.log('Database & tables created')).then(()=>{
    populateDatabases();
});

export default sequelize;