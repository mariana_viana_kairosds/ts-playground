import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

const PostModel = sequelize.define('posts', {
    id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    content: {
        type: DataTypes.STRING,
        allowNull: false,
    }
});

export { PostModel };
