/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
import { User, UserType } from '../../domain/model/entities/user.entities';
import { EmailVO } from '../../domain/model/vos/email.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { NickNameVo } from '../../domain/model/vos/nickname.vo';
import { PasswordVO } from '../../domain/model/vos/password.vo';
import { RoleVO } from '../../domain/model/vos/role.vo';
import { UserRespository } from '../../domain/repositories/user.respository';
import { UserModel } from './user.schema';

export class UserRepositoryPG implements UserRespository{
    async save(user: User): Promise<void> {
        const id= user.id.value;
        const nickName = user.nickName.value;
        const email= user.email.value;
        const password=user.password.value;
        const role = user.role.value;

        const userModel = UserModel.build({id, nickName, email, password, role});
        await userModel.save();
    }

    async getByEmail(email: EmailVO): Promise<User | null> {
        
        const user: any = await UserModel.findOne({where: {email: email.value}});
        if(!user){
            return null;
        }

        const userData: UserType = {
            id: IdVO.createWithUUID(user.id),
            nickName: NickNameVo.create(user.nickName),
            email: EmailVO.create(user.email),
            password: PasswordVO.create(user.password),
            role: RoleVO.create(user.role)

        };
        return new User(userData);
    }

    async searchById(id: IdVO): Promise<User | null> {

        const userDb: any = await UserModel.findOne({
            where: {
                id: id.value
            }
        });

        if(!userDb){
            return null;
        }

        const userData: UserType = {
            id: IdVO.createWithUUID(userDb.id),
            nickName: NickNameVo.create(userDb.nickName),
            email: EmailVO.create(userDb.email),
            password: PasswordVO.create(userDb.password),
            role: RoleVO.create(userDb.role)
        };

        return new User(userData);
    }
}