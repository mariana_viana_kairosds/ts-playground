/* eslint-disable @typescript-eslint/no-explicit-any */
import { Comment, CommentType } from '../../domain/model/entities/comment.entity';
import { Post, PostType } from '../../domain/model/entities/post.entity';
import { ExceptionWithCode } from '../../domain/model/exception-with-code';
import { ContentCommentVO } from '../../domain/model/vos/content-comments.vo';
import { ContentVO } from '../../domain/model/vos/content.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { NickNameVo } from '../../domain/model/vos/nickname.vo';
import { TimesVO } from '../../domain/model/vos/times.vo';
import { TitleVO } from '../../domain/model/vos/title.vo';
import { PostRepository } from '../../domain/repositories/post.repository';
import { CommentModel } from './comments.shema';
import { PostModel } from './post.shema';
import { UserModel } from './user.schema';

export class PostRepositoryPG implements PostRepository{
    
    async save(post: Post, userId: string): Promise<void>{
    
        const id = post.id.value;
        const title = post.title.value;
        const content = post.content.value;

        const postModel = PostModel.build({id, title, content, userId});
        await postModel.save();
    }

    async getAllPost(): Promise<Post[]> {
        const postDb: any = await PostModel.findAll({
            include: [UserModel, { model: CommentModel, include: [UserModel] }],
        });

        return postDb.map((post: any) => {
            const postData: PostType = {
                id: IdVO.createWithUUID(post.id),
                author: NickNameVo.create(post.author),
                title: TitleVO.create(post.title),
                content: ContentVO.create(post.content),
                comments: post.comments.map((dbc:any)=>{
                    const commentData: CommentType = {
                        content: ContentCommentVO.create(dbc.content),
                        id: IdVO.createWithUUID(dbc.id),
                        author: NickNameVo.create(dbc.author),
                        timestamp: TimesVO.create()
                    };
                    return new Comment(commentData);}
                )
            };
            return new Post(postData);
        });
        
    }

    async getById(id: IdVO): Promise<Post | null> {
        const postDb: any = await PostModel.findOne({
            where:{
                id: id.value
            },
            include: [UserModel, { model: CommentModel, include: [UserModel] }],
        });

        if(!postDb){
            return null;
        }
    
        const comments: Comment[] = postDb.comments.map((dbc:any) => {
            const commentData: CommentType = {
                content: ContentCommentVO.create(dbc.content),
                id: IdVO.createWithUUID(dbc.id),
                author: NickNameVo.create(dbc.author),
                timestamp: TimesVO.create()
            };
            return new Comment(commentData);
        });
        
        const postData: PostType = {
            id: IdVO.createWithUUID(postDb.id),
            author: NickNameVo.create(postDb.author),
            title: TitleVO.create(postDb.title),
            content: ContentVO.create(postDb.content),
            comments
        };
        return new Post(postData);
    }

    async getPostById(id: IdVO): Promise<Post | null>{
        const postDB: any = await PostModel.findOne({
            where: {id: id.value},
            include: [UserModel, { model: CommentModel, include: [UserModel]}],
        });

        if(!postDB) return null;

        const comments: Comment[] = postDB.comments.map((dbc:any) => {
            const commentData: CommentType = {
                id: IdVO.createWithUUID(dbc.id),
                content: ContentCommentVO.create(dbc.content),
                author: NickNameVo.create(dbc.author),
                timestamp: TimesVO.create()
            };
            return new Comment(commentData);
        });

        const postData: PostType = {
            id: IdVO.createWithUUID(postDB.id),
            author: NickNameVo.create(postDB.author),
            title: TitleVO.create(postDB.title),
            content: ContentVO.create(postDB.content),
            comments
        };
        return new Post(postData);
    }
    
    async deletePost(id: IdVO): Promise<void> {
        await PostModel.destroy({
            where: {
                id: id.value
            }
        });
    }

    async updatePost(post: Post): Promise<void> {
        await PostModel.update({
            id: post.id.value,
            title: post.title.value,
            content: post.content.value
        }, {
            where: {
                id: post.id.value
            }
        });
    }

    async addComment(post: Post, comment: Comment, userId: string): Promise<void> {
        const id = comment.id.value;
        const author = comment.author.value;
        const content = comment.content.value;

        const commentModel = CommentModel.build({id, content, author, userId, postId: post.id.value});
        await commentModel.save();
    }

    async deleteComment(postId: string, commentId: IdVO): Promise<void> {
        /*const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel)
            throw new ExceptionWithCode(404, 'User not found');*/

        const postModel: any = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel)
            throw new ExceptionWithCode(404, 'Post not found');

        const commentModel = await CommentModel.findOne({ where: { id: commentId.value}});
        if (!commentModel)
            throw new ExceptionWithCode(404, 'Comment not found');

        commentModel.destroy();
    }

    async updateComment(userId: string ,postId: string, comment: Comment): Promise<Comment | null> {
        const postModel = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel)
            throw new ExceptionWithCode(404, 'Post not found');

        const userModel = await UserModel.findOne({where:{id: userId}});

        if(!userModel) throw new ExceptionWithCode(404, 'user not found');

        const commentDB: any = await CommentModel.findOne({where: {id: comment.id.value}});

        await commentDB.update({
            author: comment.author.value,
            content: comment.content.value});

        if(!commentDB) throw new ExceptionWithCode(404, 'Comment not found');

        console.log('entra pg:', postModel, userModel, commentDB);

        return new Comment(
            {
                id: IdVO.createWithUUID(comment.id.value),
                author: NickNameVo.create(comment.author.value),
                content: ContentCommentVO.create(comment.content.value),
                timestamp: TimesVO.create()
            }
        );        
    }

    async findCommentById(commentId: IdVO): Promise<Comment | null> {
        const commentInDB: any = await CommentModel.findOne({
            where: { id: commentId.value },
            include: [UserModel],
        });

        if (!commentInDB) return null;

        return new Comment({
            id: IdVO.createWithUUID(commentInDB.id),
            author: NickNameVo.create(commentInDB.author),
            content: ContentCommentVO.create(commentInDB.content),
            timestamp: TimesVO.create()
        });

    }   
}