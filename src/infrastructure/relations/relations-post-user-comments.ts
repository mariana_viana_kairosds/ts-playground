import { CommentModel } from '../repositories/comments.shema';
import { PostModel } from '../repositories/post.shema';
import { UserModel } from '../repositories/user.schema';

export const relations = () => {
    UserModel.hasMany(PostModel);
    UserModel.hasMany(CommentModel);
    PostModel.belongsTo(UserModel);
    PostModel.hasMany(CommentModel);
    CommentModel.belongsTo(PostModel);
    CommentModel.belongsTo(UserModel);
};

