/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { SignInUseCase } from '../../application/usecases/users/sign-in.usecase';
import { SignUseCase } from '../../application/usecases/users/sign-up-usecase';
import { User } from '../../domain/model/entities/user.entities';

const router = express.Router();

router.post('/api/login', body('email').notEmpty(), body('password').notEmpty(), async (req: express.Request, res: express.Response)=>{

    try{
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()});
        }
        const useCase = Container.get(SignInUseCase);
        const {email, password} = req.body;
        const token = await useCase.execute({email, password});

        if(token){
            res.status(200).json({token});
        }else{
            res.status(401).json({error: 'Not permited'});
        }

    }catch(err: any){
        return res.status(err.code).json({error: err.message});
    }

});

router.post('/api/sign-up', body('nickName').notEmpty() ,body('email').notEmpty(), body('password').notEmpty(), body('role').notEmpty().isIn(['USER', 'AUTHOR']), async (req: express.Request, res: express.Response)=>{
    try{
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()});
        }
        const useCase = Container.get(SignUseCase);
        const {nickName, email, password, role} = req.body;

        await useCase.execute({nickName, email, password, role});
        res.status(201).json({status: 'Created'});      

    }catch(err: any){
        return res.status(err.code).json({error: err.message});
    }

});

router.get('/api/auth/role/me', passport.authenticate('jwt', { session: false }),
    (req: express.Request, res: express.Response) => {
        try {
            const user = req.user as User;
            if (user) res.status(200).json({ role: user.role.value });
        } catch (err) {
            console.log(err);
        }
    }
);

router.get('/status', (req, res) => {
    res.status(200).send('works');
});

export { router as authRouter };
