/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { IdInput } from '../../application/id.request';
import { CreateOffensiveWordUseCase } from '../../application/usecases/offensive-word/create-offensive-word.usecase';
import { DeleteOffensiveWordUseCase } from '../../application/usecases/offensive-word/delete-offensive-word.usecase';
import { GetAllOffensiveWordUseCase } from '../../application/usecases/offensive-word/get-all-offensive-word.usecase';
import { OffensiveWordRequest } from '../../application/usecases/offensive-word/offensive-word.request';
import { OffensiveWordResponse } from '../../application/usecases/offensive-word/offensive-word.response';
import { UpdateOffensiveWordUseCase } from '../../application/usecases/offensive-word/update-offensive-word.usecase';
import { Role } from '../../domain/model/vos/role.vo';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

//create
router.post('/api/offensive-word', 
    body('word').notEmpty().trim(),
    body('level').notEmpty().isNumeric(),
    async (req: express.Request, res: express.Response)=> {

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()});
        }

        const {word, level} = req.body;
        const offensiveWordRequest: OffensiveWordRequest = {
            word, level
        };
        const useCase = Container.get(CreateOffensiveWordUseCase);
        const response = await useCase.execute(offensiveWordRequest);
        return res.status(201).send(response);
    });

//read
router.get('/api/offensive-words', passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
    async (req: express.Request, res: express.Response)=>{
        const useCase = Container.get(GetAllOffensiveWordUseCase);
        const offensiveWords: OffensiveWordResponse[] = await useCase.execute();
        return res.status(200).json(offensiveWords);
    });

//update
router.patch('/api/offensive-word/:id', 
    async (req, res)=>{
        try{
            const id : IdInput = req.params.id;
            const {word, level} = req.body;
            const offensiveWordRequest: OffensiveWordRequest = {word, level};
            const useCase = Container.get(UpdateOffensiveWordUseCase);
            await useCase.execute(id, offensiveWordRequest);
            return res.status(200).json({id: id,word: offensiveWordRequest.word, level: offensiveWordRequest.level});
        } catch(err: any){
            return res.status(404).json({error: err.message});
        }
    });

//delete
router.delete('/api/offensive-word/:id', async (req, res)=>{
    try{
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({error: errors.array()});
        }
        const idDelete: IdInput = req.params?.id;
        const useCase = Container.get(DeleteOffensiveWordUseCase);
        await useCase.execute(idDelete);
        res.status(200).json('deleted'); 

    } catch(err: any){
        return res.status(err.code).json({error: err.message});
    }
});

export { router as offensiveWordRouter };
