/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { NextFunction } from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { IdInput } from '../../application/id.request';
import { CreatePostRequest, CreatePostResponse } from '../../application/types/posts';
import { CommentInput, CreateCommentUseCase } from '../../application/usecases/comments/create-comments.usecase';
import { DeleteCommentUseCase } from '../../application/usecases/comments/delete-comment.usecase';
import { UpdateCommentUseCase } from '../../application/usecases/comments/update-comment.usecase';
import {
    CreatePostUseCase
} from '../../application/usecases/posts/create-posts.usecase';
import { DeletePostUseCase } from '../../application/usecases/posts/delete-post.usecase';
import { GetAllPostUseCase } from '../../application/usecases/posts/getAllPost.usecase';
import { GetPostCommentsUseCase } from '../../application/usecases/posts/getPost-comments.usecase';
import { UpdatePostUseCase } from '../../application/usecases/posts/update-post.usecase';
import { User } from '../../domain/model/entities/user.entities';
import { Role } from '../../domain/model/vos/role.vo';
import { cache, updateCache } from '../cache/middleware';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

//crear un post
router.post(
    '/api/posts',
    body('title').notEmpty().isString(),
    body('content').notEmpty().isString(),
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]),
    async (req: express.Request, res: express.Response) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }
            const user = req.user as User;
            const useCase = Container.get(CreatePostUseCase);

            const request: CreatePostRequest = {
                author: req.body.author,
                title: req.body.title,
                content: req.body.content,
            };
            const response = await useCase.execute(request, user.id.value);
            return res.status(201).json(response);
        } catch (err: any) {
            return res.status(err.code).json({ error: err.message });
        }
    }
);

router.get('/api/posts', cache(), 
    async(req: express.Request, res: express.Response) => {
        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            const useCase = Container.get(GetAllPostUseCase);
            const posts : CreatePostResponse[] = await useCase.execute();
            res.locals.data = posts;
            return res.status(200).json(posts);
        }catch (err: any){
            return res.status(500).json({error: err.message});
        }
    }
);

router.get('/api/posts/:idPost',cache(), 
    async(req: express.Request, res: express.Response,  next: NextFunction
    ) => {
        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }

            const id: IdInput = req.params.idPost;
            const useCase = Container.get(GetPostCommentsUseCase);
            const posts = await useCase.execute(id);
            res.locals.data = posts;
            return res.status(200).json(posts);
        }catch (err: any){
            return res.status(500).json({error: err.message});
        }
    });

router.patch('/api/posts/:idPost', updateCache(),passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]), async(req: express.Request, res: express.Response,  next: NextFunction
    ) => {
        try{
            const useCase = Container.get(UpdatePostUseCase);
            const id: IdInput = req.params.idPost;
            const { title, content, author } = req.body;
            const post = await useCase.execute(id, { title, content, author });
            return res.status(200).json(post);
        } catch (err) {
            return next(err);
        }
    });

router.delete('/api/posts/:idPost', passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]), async(req: express.Request, res: express.Response, next: NextFunction) => {
        try{
            const useCase = Container.get(DeletePostUseCase);
            const id: IdInput = req.params.idPost;
            await useCase.execute(id);
            return res.status(204).send();
        }catch (err){
            return next(err);
        }
    });

router.post(
    '/api/posts/:idPost/comment',
    body('author').notEmpty(),
    body('content').notEmpty(),
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    async (req: express.Request, res: express.Response) => {

        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }

            const idPost = req.params.idPost;
            const user = req.user as User;
      
            const useCase = Container.get(CreateCommentUseCase);
      
            const request: CommentInput = {
                postId: idPost,
                author: req.body.author,
                content: req.body.content
            };
  
            const response = await useCase.execute(request, user.id.value,user.email.value);
            return res.status(201).json(response);
        }catch (err: any){
            return res.status(500).json({error: err.message});
        }

    }
);

router.delete('/api/posts/:idPost/comment/:idComment', passport.authenticate('jwt', {session: false}), 
    hasRole([Role.AUTHOR, Role.ADMIN, Role.USER]),
    async(req: express.Request, res: express.Response) => {
        try{
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const idPost: IdInput = req.params.idPost;
            const idComment: IdInput = req.params.idComment;

            const useCase = Container.get(DeleteCommentUseCase);

            await useCase.execute(idPost,idComment);

            res.status(204).send();

        }catch(err: any){
            return res.status(500).json({error:err.message});
        }
    }
);

router.patch('/api/posts/:idPost/comment/:idComment',passport.authenticate('jwt', {session: false}), 
    hasRole([Role.AUTHOR, Role.ADMIN, Role.USER]) ,async(req: express.Request, res: express.Response,  next: NextFunction
    ) => {
        try{
            const postId = req.params.idPost;
            const idComment = req.params.idComment;
            const user = req.user as User;

            const useCase = Container.get(UpdateCommentUseCase);

            console.log('routes:', postId, idComment, user);

            const {author, content } = req.body;

            const post = await useCase.execute(user.id.value,postId, idComment, {author, content});
        
            return res.status(201).json(post);
        } catch(err: any){
            console.log(err);
            return res.status(500).json({error:err.message});
        }
    });

export { router as PostRouter };
