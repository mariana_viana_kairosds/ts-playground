import mongoose from 'mongoose';

const CacheSchema = new mongoose.Schema({
    key: {
        type: String,
        required: true
    },
    data: {
        type: String,
        required: true
    }
});

export const CacheModel = mongoose.model('Cache', CacheSchema);