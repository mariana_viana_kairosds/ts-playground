import { Service } from 'typedi';
import { CacheModel } from './cache.shema';

export interface Cache {
    key: string;
    data: string;
}

@Service()
export class CacheRepositoryMongo {

    async save(input: Cache): Promise<void> {
        const { key, data } = input;
        const cacheModel = new CacheModel({ key, data });
        await cacheModel.save();
    }

    async getByKey(key: string): Promise<string | null> {
        const cacheDB = await CacheModel.findOne({ key: key }).exec();

        if (!cacheDB) {
            return null;
        }

        return cacheDB.data;
    }

    async delete(key: string): Promise<void> {
        await CacheModel.findOneAndDelete({ key: key });
    }

    async deleteAll(): Promise<void> {
        await CacheModel.deleteMany({});
    }

}
