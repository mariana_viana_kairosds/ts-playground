import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { GetAllPostUseCase } from '../../application/usecases/posts/getAllPost.usecase';
import { Cache, CacheRepositoryMongo } from './cache.repository.mongo';

const EXPIRATION_TIME = 60;

const saveData = async ({key, data}: Cache) => {
    const cacheRepository = Container.get(CacheRepositoryMongo);
    await cacheRepository.save({key, data});

    setTimeout(async () => {
        await cacheRepository.delete(key);
    }, EXPIRATION_TIME * 3000);
};

export const cache = () => {
    return async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        const cacheRepository = Container.get(CacheRepositoryMongo);
        const key = req.originalUrl;
        const data: string | null = await cacheRepository.getByKey(key);

        if (data) {

            return res.status(200).json(JSON.parse(data));
        }

        res.on('finish', async () => {
            const { data } = res.locals;

            if (data) {
                saveData({ key, data: JSON.stringify(data) });
            }

        });

        next();
    };
};

export const updateCache = () => {
    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        res.on('finish', async () => {
            const cacheRepository = Container.get(CacheRepositoryMongo);
            await cacheRepository.deleteAll();

            const getAllPostsUseCase = Container.get(GetAllPostUseCase);
            const posts = await getAllPostsUseCase.execute();

            await saveData({ key: '/api/v1/post/', data: JSON.stringify(posts) });
        });

        next();
    };
};
