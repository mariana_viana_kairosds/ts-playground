import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';

export interface OffensiveWordRepository {

    save(offensiveWord: OffensiveWord): Promise<void>;

    getAll(): Promise<OffensiveWord[]>;

    update(offensiveWord: OffensiveWord): Promise<void>;

    delete(id: IdVO): Promise<void>;

    getById(id: IdVO): Promise<OffensiveWord | null>

}