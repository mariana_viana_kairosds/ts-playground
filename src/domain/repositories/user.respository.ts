import { User } from '../model/entities/user.entities';
import { EmailVO } from '../model/vos/email.vo';
import { IdVO } from '../model/vos/id.vo';

export interface UserRespository{

    save(user: User): Promise<void>;

    getByEmail(email: EmailVO): Promise<User | null>;

    searchById(id: IdVO): Promise<User | null>;

}