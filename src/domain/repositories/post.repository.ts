import { Comment, CommentType } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';

export interface PostRepository {

    //guardar
    save(post: Post, userId: string): Promise<void>;

    //obtenerlos todos
    getAllPost(): Promise<Post[]>;

    getPostById(id: IdVO): Promise<Post | null>;

    getById(id: IdVO): Promise<Post | null>;

    //borrrar los post
    deletePost(id: IdVO): Promise<void>;

    //modificar
    updatePost(post: Post): Promise<void>;

    //obtener un comentario
    //getComment(idComment: IdVO): Promise<void>;

    //agregar comentario
    addComment(post: Post, comment: Comment, userId: string): Promise<void>;

    //borrar comentario
    deleteComment(postId: string, commentId: IdVO): Promise<void>;

    updateComment(userId: string,postId: string, Comment: CommentType): Promise<Comment | null>;

    findCommentById(commentId: IdVO): Promise<Comment | null>;
}