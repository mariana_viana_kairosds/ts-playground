import { EmailVO } from '../vos/email.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVo } from '../vos/nickname.vo';
import { PasswordVO } from '../vos/password.vo';
import { RoleVO } from '../vos/role.vo';

export type UserType = {
    id: IdVO;
    nickName: NickNameVo;
    email: EmailVO;
    password: PasswordVO;
    role: RoleVO;
};

export class User {
 
    constructor(private user: UserType){}

    get id(): IdVO{
        return this.user.id;
    }

    get nickName(): NickNameVo{
        return this.user.nickName;
    }

    get email(): EmailVO{
        return this.user.email;
    }

    get password(): PasswordVO{
        return this.user.password;
    }

    get role(): RoleVO {
        return this.user.role;
    }

}