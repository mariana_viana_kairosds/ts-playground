import { ContentCommentVO } from '../vos/content-comments.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVo } from '../vos/nickname.vo';
import { TimesVO } from '../vos/times.vo';

export type CommentType = {
    id: IdVO;
    author: NickNameVo;
    content: ContentCommentVO;
    timestamp: TimesVO;
}

export class Comment {

    constructor(private comment: CommentType) {}

    get id(): IdVO {
        return this.comment.id;
    }

    get author(): NickNameVo {
        return this.comment.author;
    }

    get content(): ContentCommentVO {
        return this.comment.content;
    }

    get timestamp(): TimesVO {
        return this.comment.timestamp;
    }
}