import { ContentVO } from '../vos/content.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVo } from '../vos/nickname.vo';
import { TitleVO } from '../vos/title.vo';
import { Comment } from './comment.entity';

export type PostType = {
    id: IdVO;
    author: NickNameVo;
    title: TitleVO;
    content: ContentVO;
    comments: Comment[];
}

export class Post {
    constructor(private post: PostType){}

    get id(): IdVO {
        return this.post.id;
    }

    get title(): TitleVO {
        return this.post.title;
    }

    get content(): ContentVO {
        return this.post.content;
    }

    get author(): NickNameVo {
        return this.post.author;
    }

    get comments(): Comment[]{
        return this.post.comments;
    }

    addComments(comment: Comment): void{
        this.post.comments = [...this.post.comments, comment];
    }

    deleteComment(idComment: IdVO): void {
        this.post.comments = this.post.comments.filter(c => c.id.value !== idComment.value);
    }

    getCommentById(idComment: IdVO): Comment | undefined {
        return this.post.comments.find(c => c.id.value === idComment.value);
    }

}