import { IdVO } from '../vos/id.vo';
import { LevelVO } from '../vos/level.vo';
import { WordVO } from '../vos/word.vo';

export type OffensiveWordType = {
    id: IdVO;
    word: WordVO;
    level: LevelVO;
};

export class OffensiveWord {

    constructor(private offensiveWordType: OffensiveWordType) {}

    get id(): IdVO {
        return this.offensiveWordType.id;
    }
    get word(): WordVO {
        return this.offensiveWordType.word;
    }
    get level(): LevelVO {
        return this.offensiveWordType.level;
    }
}
