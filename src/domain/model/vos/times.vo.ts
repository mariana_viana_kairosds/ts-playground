export class TimesVO {
    private times: string;

    get value(): string {
        return this.times;
    }

    private constructor(){
        this.times = Date.now().toString();
    }

    static create(): TimesVO {
        return new TimesVO();
    }
}