export class NickNameVo{
    get value(): string {
        return this.nickName;
    }

    private constructor(private nickName: string){}

    static create(nickName: string): NickNameVo{return new NickNameVo(nickName);}
}