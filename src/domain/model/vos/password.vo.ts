export class PasswordVO {
    get value(){
        return this.password;
    }

    private constructor(private password: string){}

    static createFromHash(password: string) {
        return new PasswordVO(password);
    }

    static create(password: string): PasswordVO{
        return new PasswordVO(password);
    }
}