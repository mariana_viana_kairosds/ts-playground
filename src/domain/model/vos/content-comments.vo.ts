import { checkOffensiveWords } from '../../services/checkOffensiveWords';
import { OffensiveWord } from '../entities/offensive-word.entity';
import { ExceptionWithCode } from '../exception-with-code';

export class ContentCommentVO {
    private static readonly MIN_LENGTH = 10;
    private static readonly MAX_LENGTH = 200;

    get value(): string {
        return this.content;
    }

    private constructor(private content: string) {}

    static create(content: string, offensiveWords: OffensiveWord[]=[]): ContentCommentVO {
        const long = content.length;

        if(long < this.MIN_LENGTH){
            throw new ExceptionWithCode(400, `You are missing ${this.MIN_LENGTH - long} characters`);
        }
        if(long > this.MAX_LENGTH){
            throw new ExceptionWithCode(400, `You have plenty ${this.MAX_LENGTH - long} characters`);
        }
        const offensiveWordsFound = checkOffensiveWords(content, offensiveWords);
        if (offensiveWordsFound.length > 0) {
            //logger.debug(JSON.stringify(offensiveWordsFound));
            throw new ExceptionWithCode(400, 'Exist offensive words ');
        }

        return new ContentCommentVO(content);
    }

}