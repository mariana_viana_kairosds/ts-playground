import { ExceptionWithCode } from '../exception-with-code';

export class TitleVO {

    private static readonly MIN_LENGTH = 5;
    private static readonly MAX_LENGTH = 30;    

    get value(): string {
        return this.title;
    }

    private constructor(private title: string){}

    static create(title: string): TitleVO {
        if(!title){
            throw new ExceptionWithCode(400, 'The title is required');
        }
        if(title.length < this.MIN_LENGTH){
            throw new ExceptionWithCode(400, `The title cannot be less than ${this.MIN_LENGTH} characters `);
        }
        if(title.length > this.MAX_LENGTH){
            throw new ExceptionWithCode(400, `The title cannot be higher than ${this.MAX_LENGTH} characters`);
        }
        return new TitleVO(title);
    }
}