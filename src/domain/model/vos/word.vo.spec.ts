import { WordVO } from './word.vo';

describe('Word VO', ()=>{

    it('should create', ()=> {
        const newWord = 'world';
        const word = WordVO.create(newWord);
        expect(word.value).toEqual(newWord);
    });

});