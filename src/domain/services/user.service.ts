import bcrypt from 'bcrypt';
import { Inject, Service } from 'typedi';
import { User, UserType } from '../model/entities/user.entities';
import { EmailVO } from '../model/vos/email.vo';
import { IdVO } from '../model/vos/id.vo';
import { PasswordVO } from '../model/vos/password.vo';
import { UserRespository } from '../repositories/user.respository';

@Service()

export class UserService {

    constructor(@Inject('UserRepository') private UserRepository: UserRespository){}

    async isValidPassword(password: PasswordVO, user: User): Promise<boolean>{
        return bcrypt.compare(password.value, user.password.value);
    }

    async persist(user: User): Promise<void>{
        
        const hash = await bcrypt.hash(user.password.value, 10);
        const encryptPassWord = PasswordVO.create(hash);
        const newUser: UserType = {
            id: user.id,
            nickName: user.nickName,
            email: user.email,
            password: encryptPassWord,
            role: user.role
        };
        await this.UserRepository.save(new User(newUser));
    }

    async getByEmail(email: EmailVO): Promise<User |null>{
        return this.UserRepository.getByEmail(email);
    }

    async getById(id: IdVO): Promise<User | null> {
        return this.UserRepository.searchById(id);
    }
}