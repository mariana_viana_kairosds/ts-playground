import { Inject, Service } from 'typedi';
import { Comment, CommentType } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { ExceptionWithCode } from '../model/exception-with-code';
import { IdVO } from '../model/vos/id.vo';
import { PostRepository } from '../repositories/post.repository';

@Service()

export class PostService {

    constructor(@Inject('PostRepository') private postRepository: PostRepository) {}

    async persist(post: Post, userId: string): Promise<void> {
        const postEntity = new Post(post);
        return this.postRepository.save(postEntity, userId);
    }

    async getById(postId: IdVO): Promise<Post | null> {
        return this.postRepository.getById(postId);
    }

    async getAllPost(): Promise<Post[]>{
        return this.postRepository.getAllPost();
    }

    async getPostById(id: IdVO): Promise<Post | null>{
        return this.postRepository.getPostById(id);
    }

    async deletePost(postId: IdVO): Promise <void>{
        await this.postRepository.deletePost(postId);
    }

    async updatePost(post: Post): Promise<void>{
        await this.postRepository.updatePost(post);
    }

    async addComment(post: Post, comment: Comment, userId: string): Promise<void> {
        post.addComments(comment);
        return this.postRepository.addComment(post,comment,userId);
    }

    async updateComment(userId: string,postId: string, comment: CommentType): Promise<Comment | null> {
        await this.findComment(comment.id);
        //await this.getPostById(postId);
        return this.postRepository.updateComment(userId, postId, new Comment(comment));
    }

    async deleteComment(postId: string, commentId: IdVO): Promise<void> {
        //await this.getPostById(postId);
        await this.findComment(commentId);
        return this.postRepository.deleteComment(postId, commentId);
    }

    async findComment(commentId: IdVO): Promise<Comment> {
        const comment = await this.postRepository.findCommentById(commentId);

        if (!comment)
            throw new ExceptionWithCode(404, 'Comment not found');

        return comment;
    }
}