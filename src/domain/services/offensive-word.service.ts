import { Inject, Service } from 'typedi';
import { OffensiveWord, OffensiveWordType } from '../model/entities/offensive-word.entity';
import { ExceptionWithCode } from '../model/exception-with-code';
import { IdVO } from '../model/vos/id.vo';
import { LevelVO } from '../model/vos/level.vo';
import { WordVO } from '../model/vos/word.vo';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';

@Service()

export class OffensiveWordService {
    constructor(@Inject('OffensiveWordRepository') private OffensiveWordRepository: OffensiveWordRepository){}

    async persist(offensiveWord: OffensiveWordType): Promise<void>{
        const offensiveWordEntity = new OffensiveWord(offensiveWord);
        return this.OffensiveWordRepository.save(offensiveWordEntity);
    }

    async getAll(): Promise<OffensiveWord[]>{
        return this.OffensiveWordRepository.getAll();
    }

    async update(offensiveWord: OffensiveWord): Promise<void>{
        await this.checkIfIdExist(offensiveWord.id);
        const IdOriginal = await this.OffensiveWordRepository.getById(offensiveWord.id);
        const offensiveWordMerge: OffensiveWordType = {
            id: offensiveWord.id,
            word: WordVO.create(offensiveWord.word.value ?? IdOriginal?.word.value),
            level: LevelVO.create(offensiveWord.level.value ?? IdOriginal?.level.value)
        };
        await this.OffensiveWordRepository.update(new OffensiveWord(offensiveWordMerge));
    }

    async delete(id: IdVO): Promise<void> {
        const offensiveWord = await this.checkIfIdExist(id);
        await this.OffensiveWordRepository.delete(offensiveWord.id);
    }

    async getById(id: IdVO): Promise<OffensiveWord | null>{
        return this.OffensiveWordRepository.getById(id);
    }

    private async checkIfIdExist(id: IdVO): Promise<OffensiveWord> {
        const offensiveWordId = await this.getById(id);
        if(!offensiveWordId){
            throw new ExceptionWithCode(404, `Id ${id} not found`);
        }
        return offensiveWordId;
    }
}

