/* eslint-disable @typescript-eslint/no-unused-vars */
import { json } from 'body-parser';
import cors from 'cors';
import express from 'express';
import expressPinoLogger from 'express-pino-logger';
import passport from 'passport';
import 'reflect-metadata';
import swaggerJsdoc, { Options } from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';
import { Container } from 'typedi';
import { logger } from './infrastructure/config/logger';
import { connectToDB } from './infrastructure/config/mongo';
import './infrastructure/config/postgresql';
import passportMiddleware from './infrastructure/middlewares/passport';
import { relations } from './infrastructure/relations/relations-post-user-comments';
import { OffensiveWordRepositoryMongo } from './infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from './infrastructure/repositories/post.repository.pg';
import { UserRepositoryPG } from './infrastructure/repositories/user.repositories.pg';
import { authRouter } from './infrastructure/routes/auth.routes';
import { offensiveWordRouter } from './infrastructure/routes/offensive-word.route';
import { PostRouter } from './infrastructure/routes/post.routes';

Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());
Container.set('UserRepository', new UserRepositoryPG());
Container.set('PostRepository', new PostRepositoryPG());

connectToDB();

console.log('App started');

const app = express();

app.use(json());

app.use(cors());
app.use(expressPinoLogger(logger));
app.use(offensiveWordRouter);
app.use(authRouter);
app.use(PostRouter);
app.use(passport.initialize());
passport.use(passportMiddleware);

const options: Options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Blog API',
            version: '1.0.0',
            description: 'Blog API'
        },
        servers: [
            {
                url: 'http://localhost:3000/api'
            }
        ],
    },
    apis: ['./src/infrastructure/routes/*.ts']
};

const spec = swaggerJsdoc(options);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(spec));

app.use((error: express.ErrorRequestHandler,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction) => {
    console.log(error);
    res.status(500).send('internal server error');
});

app.listen(3000, ()=>{
    relations();
    console.log('server started');
});
