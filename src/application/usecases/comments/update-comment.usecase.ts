import { Service } from 'typedi';
import { CommentType } from '../../../domain/model/entities/comment.entity';
import { ContentCommentVO } from '../../../domain/model/vos/content-comments.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TimesVO } from '../../../domain/model/vos/times.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { PostService } from '../../../domain/services/post.service';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class UpdateCommentUseCase {
    constructor(
        private postService: PostService,
        private offensiveWordService: OffensiveWordService,
        private userService: UserService
    ) {}

    async execute(userId: string ,postId: string, commentId: string, comment: UpdateCommentInput): Promise<CommentOutput> {
        const commentType: CommentType = {
            id: IdVO.createWithUUID(commentId),
            author: NickNameVo.create(comment.author),
            content: ContentCommentVO.create(comment.content),
            timestamp: TimesVO.create()
        };

        await this.postService.updateComment(
            userId, postId, commentType
        );

        return {
            id: commentType.id.value,
            author: commentType.author.value,
            content: commentType.content.value
        };
    }
}

export type CommentOutput = {
    id: string;
    author: string;
    content: string;
}

export type UpdateCommentInput = {
    author: string;
    content: string;
}