import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { UserService } from '../../../domain/services/user.service';
import { IdInput } from '../../id.request';

@Service()

export class DeleteCommentUseCase {
    constructor(private postService: PostService, private userService: UserService){}

    async execute(postId: IdInput, commentId: IdInput): Promise<void> {
        await this.postService.deleteComment(
            postId,
            IdVO.createWithUUID(commentId),
        );
    }
}

