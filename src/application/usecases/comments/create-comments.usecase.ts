import { Service } from 'typedi';
import {
    Comment,
    CommentType
} from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { User } from '../../../domain/model/entities/user.entities';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { ContentCommentVO } from '../../../domain/model/vos/content-comments.vo';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TimesVO } from '../../../domain/model/vos/times.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { PostService } from '../../../domain/services/post.service';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class CreateCommentUseCase {
    constructor(
        private postService: PostService,
        private userService: UserService,
        private offensiveWordService: OffensiveWordService
    ) {}

    async execute(
        request: CommentInput,
        userId: string,
        email: string
    ): Promise<CommentOutput> {
        const post: Post | null = await this.postService.getById(
            IdVO.createWithUUID(request.postId)
        );

        if (!post) {
            throw new ExceptionWithCode(404, 'Post not found');
        }

        const author: User | null = await this.userService.getByEmail(
            EmailVO.create(email)
        );

        if (!author) {
            throw new ExceptionWithCode(404, 'Author not found');
        }

        const offensiveWords = await this.offensiveWordService.getAll();

        const commentData: CommentType = {
            id: IdVO.create(),
            author: NickNameVo.create(request.author),
            content: ContentCommentVO.create(request.content, offensiveWords),
            timestamp: TimesVO.create(),
        };

        await this.postService.addComment(post, new Comment(commentData), userId);

        return {
            idComment: commentData.id.value,
            content: commentData.content.value
        };
    }
}

export type CommentInput = {
    postId: string;
    author: string;
    content: string;
};

export type CommentOutput = {
    idComment: string;
    content: string;
};
