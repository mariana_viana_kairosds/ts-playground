import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { DeleteOffensiveWordUseCase } from './delete-offensive-word.usecase';


jest.mock('./../../../infrastructure/repositories/offensive-word.repository.mongo');

describe('Delete offensive word Use Case', () => {

    it('should delete offensive word', async ()=> {
        const respository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', respository);

        jest.spyOn(respository, 'getById').mockResolvedValue(
            new OffensiveWord({
                id: IdVO.createWithUUID('eecfe194-5d2e-4940-b69e-71050257df02'),
                word: WordVO.create('Test'),
                level: LevelVO.create(3),
            })
        );
        jest.spyOn(respository, 'delete').mockResolvedValue();
        
        const useCase: DeleteOffensiveWordUseCase = Container.get(DeleteOffensiveWordUseCase);
        
        await useCase.execute('eecfe194-5d2e-4940-b69e-71050257df02');
        expect(respository.delete).toHaveBeenCalled();
    });
});