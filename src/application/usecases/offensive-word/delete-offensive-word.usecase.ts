import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { IdInput } from '../../id.request';

@Service()

export class DeleteOffensiveWordUseCase{
    constructor(private offensiveWordService: OffensiveWordService){}

    async execute(IdInput: IdInput): Promise<void>{

        const id = IdVO.createWithUUID(IdInput);
        
        await this.offensiveWordService.delete(id);
    }
}