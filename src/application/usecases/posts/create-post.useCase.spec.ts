jest.mock('./../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                save: jest.fn(),
                searchById: jest.fn(),
            };
        }),
    };
});


import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { CreatePostRequest } from '../../types/posts';
import { PostRepositoryPG } from './../../../infrastructure/repositories/post.repository.pg';
import { CreatePostUseCase } from './create-posts.usecase';

describe('Create post', () => {
    it('should create post Use Case', async () => {

        const repository = new PostRepositoryPG();

        Container.set('PostRepository', repository);
        Container.set('UserRepository', repository);

        const useCase = Container.get(CreatePostUseCase);
        const userId= IdVO.create().value;
        const post: CreatePostRequest = {
            author: 'showWoman',
            title: 'El circo sale',
            content: 'Es cierto que muchas obras dejan que desear, pero no es el caso de esta, ya que se sabe llegar al expectador y transmitir las maravillas del circo'
        };

        await useCase.execute(post, userId);
        expect(repository.save).toHaveBeenCalled();
    });
});