import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { UserService } from '../../../domain/services/user.service';
import { CreatePostRequest, CreatePostResponse } from '../../types/posts';

@Service()
export class CreatePostUseCase {
    constructor(
        private postService: PostService, private userService: UserService
    ) {}

    async execute(request: CreatePostRequest, userId: string): Promise<CreatePostResponse> {

        const postData: PostType = {
            id: IdVO.create(),
            title: TitleVO.create(request.title),
            content: ContentVO.create(request.content),
            author: NickNameVo.create(request.author),
            comments: [],
        };
        await this.postService.persist(new Post(postData), userId);
        return {
            id: postData.id.value,
            title: postData.title.value,
            content: postData.content.value,
            author: postData.author.value,
        };
    }
}
