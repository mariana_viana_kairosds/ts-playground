jest.mock('./../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(()=> {
            return {
                getAllPost:jest.fn().mockImplementation(() => [new Post({
                    id: IdVO.create(),
                    author: NickNameVo.create('showWoman'),
                    title: TitleVO.create('el circo'),
                    content: ContentVO.create('Es cierto que muchas obras dejan que desear, pero no es el caso de esta, ya que se sabe llegar al expectador y transmitir las maravillas del circo'),
                    comments: []
                })])};
        })
        
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { GetAllPostUseCase } from './getAllPost.usecase';
describe('Get all post Use Case', () => {
    it('should get all post from respository', async() => {
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(GetAllPostUseCase);

        const postAll = await useCase.execute();
        expect(repository.getAllPost).toHaveBeenCalled();
        expect(postAll[0].author).toEqual('showWoman');
    });
});