jest.mock('./../../../infrastructure/repositories/post.repository.pg', () =>{
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() =>{
            return {
                deletePost: jest.fn(),
                getPostById: jest.fn()
            };
        })
    };
});


import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { IdInput } from '../../id.request';
import { PostRepositoryPG } from './../../../infrastructure/repositories/post.repository.pg';
import { DeletePostUseCase } from './delete-post.usecase';

describe('Delete posts use case', () => {
    test('should delete posts', async () => {
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const postId: IdInput = IdVO.create().value;

        const useCase: DeletePostUseCase = Container.get(DeletePostUseCase);

        await useCase.execute(postId);
        expect(repository.deletePost).toHaveBeenCalled();
    });
});
