import { Service } from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { PostService } from '../../../domain/services/post.service';
import { CreatePostResponse } from '../../types/posts';

@Service()

export class GetAllPostUseCase {
    constructor(private postService: PostService){}

    async execute(): Promise<CreatePostResponse[]> {
        const posts: Post[] = await this.postService.getAllPost();
        
        const postResponse: CreatePostResponse[] = posts.map(post => {
            return {
                id: post.id.value,
                title: post.title.value,
                content: post.content.value,
                author: post.author.value
            };
        });
        return postResponse;
    }
}