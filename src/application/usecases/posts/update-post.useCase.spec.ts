jest.mock('./../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(()=> {
            return {
                getById:jest.fn().mockImplementation(() => {new Post({
                    id: IdVO.createWithUUID('eecfe194-5d2e-4940-b69e-71050257df02'),
                    author: NickNameVo.create('showWoman'),
                    title: TitleVO.create('el circo'),
                    content: ContentVO.create('Es cierto que muchas obras dejan que desear, pero no es el caso de esta, ya que se sabe llegar al expectador y transmitir las maravillas del circo'),
                    comments: []
                });}),
                updatePost: jest.fn()
            };
        })
        
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { UpdatePostUseCase } from './update-post.usecase';
describe('Get all post Use Case', () => {
    it('should get all post from respository', async() => {
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(UpdatePostUseCase);

        await useCase.execute('eecfe194-5d2e-4940-b69e-71050257df02', {
            author: 'showWoman',
            title: 'el circo',
            content:'Es cierto que muchas obras dejan que desear, pero no es el caso de esta, ya que se sabe llegar al expectador y transmitir las maravillas del circo',
        });
        expect(repository.updatePost).toHaveBeenCalled();
    });
});