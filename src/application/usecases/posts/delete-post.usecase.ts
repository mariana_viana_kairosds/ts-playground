import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdInput } from '../../id.request';

@Service()

export class DeletePostUseCase {
    constructor(private postService: PostService){}

    async execute(id: IdInput): Promise<void>{
        await this.postService.deletePost(IdVO.createWithUUID(id));
    }
    
}