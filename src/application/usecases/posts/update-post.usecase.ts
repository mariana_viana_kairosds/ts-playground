import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdInput } from '../../id.request';
import { CreatePostRequest } from '../../types/posts';


@Service()
export class UpdatePostUseCase {
    constructor(private postService: PostService) {}

    async execute(
        id: IdInput,
        postRequest: CreatePostRequest
    ): Promise<PostResponse> {

        const postData: PostType = {
            id: IdVO.createWithUUID(id),
            title: TitleVO.create(postRequest.title),
            content: ContentVO.create(postRequest.content),
            author: NickNameVo.create(postRequest.author),
            comments: []
        };

        await this.postService.updatePost(new Post(postData));

        return {
            id: postData.id.value,
            title: postData.title.value,
            content: postData.content.value,
            author: postData.author.value,
        };
    }
}

export type PostResponse = {
    id: string;
    title: string;
    content: string;
    author: string;
};

export type CommentResponse = {
    id: string;
    author: string;
    content: string
}


