import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdInput } from '../../id.request';
import { PostWithCommentsResponse } from '../../types/posts';

@Service()

export class GetPostCommentsUseCase {
    constructor(private postService: PostService){}

    async execute(id: IdInput): Promise<PostWithCommentsResponse> {
        const post = await this.postService.getById(IdVO.createWithUUID(id));
        
        return {
            id: post?.id.value,
            title: post?.title.value,
            content: post?.content.value,
            author: post?.author.value,
            comments: post?.comments.map(comment => {
                return {
                    id: comment.id.value,
                    author: comment.author.value,
                    content: comment.content.value
                };
            })
        };
    }
}