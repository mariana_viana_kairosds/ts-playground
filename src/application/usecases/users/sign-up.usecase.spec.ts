jest.mock('./../../../infrastructure/repositories/user.repositories.pg', () => {
    return {
        UserRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                save: jest.fn()
            };
        }),
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user.repositories.pg';
import { SignUpRequest, SignUseCase } from './sign-up-usecase';

describe('Login user', () =>{
    it('user should sign up', (async () => {
        const repository = new UserRepositoryPG();
        Container.set('UserRepository', repository);
        const useCase = Container.get(SignUseCase);
        const signUp: SignUpRequest = {
            nickName: 'Vianam',
            email: 'viana@gmail.com',
            password: '1235',
            role: 'AUTHOR'
        };

        await useCase.execute(signUp);
        expect(repository.save).toHaveBeenCalled();
    }));
});

