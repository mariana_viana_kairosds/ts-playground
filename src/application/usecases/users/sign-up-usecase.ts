import { Service } from 'typedi';
import { User, UserType } from '../../../domain/model/entities/user.entities';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/role.vo';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class SignUseCase {

    constructor(private userService: UserService){}

    async execute(request: SignUpRequest): Promise<void> {
        
        const user: UserType = {
            id: IdVO.create(),
            nickName: NickNameVo.create(request.nickName),
            email: EmailVO.create(request.email),
            password: PasswordVO.create(request.password),
            role: RoleVO.create(Role[request.role])
        };
        await this.userService.persist(new User(user));
    }
}

export type SignUpRequest = {
    nickName: string;
    email: string;
    password: string;
    role: 'USER' | 'AUTHOR';
}