import jwt from 'jsonwebtoken';
import { Service } from 'typedi';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class SignInUseCase {
    constructor(private userservice: UserService) {}

    async execute(request: SignInRequest): Promise<string | null> {
        const user = await this.userservice.getByEmail(
            EmailVO.create(request.email)
        );
        if (!user) {
            throw new ExceptionWithCode(404, 'User not found');
        }
        const plainPassword = PasswordVO.create(request.password);
        const isValid = await this.userservice.isValidPassword(plainPassword, user);
        if (isValid) {
            return jwt.sign({ email: user?.email.value }, 'secret', {
                expiresIn: 86400, // 24horas
            });
        }
        return null;
    }
}

export type SignInRequest = {
    email: string;
    password: string;
};
