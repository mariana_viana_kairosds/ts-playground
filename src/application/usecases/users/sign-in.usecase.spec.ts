jest.mock('./../../../infrastructure/repositories/user.repositories.pg', () => {
    return {
        UserRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                getByEmail: jest.fn().mockImplementation(()=>{
                    return new User({
                        id: IdVO.create(),
                        nickName: NickNameVo.create('Vianam'),
                        email: EmailVO.create('viana@gmail.com'),
                        password: PasswordVO.create('1235'),
                        role: RoleVO.create(Role.USER)
                    });
                }),
            };
        }),
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../../domain/model/entities/user.entities';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { NickNameVo } from '../../../domain/model/vos/nickname.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/role.vo';
import { UserRepositoryPG } from './../../../infrastructure/repositories/user.repositories.pg';
import { SignInRequest, SignInUseCase } from './sign-in.usecase';

describe('Login user', () =>{
    it('user should login', (async () => {
        const repository = new UserRepositoryPG();
        Container.set('UserRepository', repository);
        const useCase = Container.get(SignInUseCase);
        const signIn: SignInRequest = {
            email: 'viana@gmail.com',
            password: '1235',
        };

        await useCase.execute(signIn);
        expect(repository.getByEmail).toHaveBeenCalled();

    }));
});

