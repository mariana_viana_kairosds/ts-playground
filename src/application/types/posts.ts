import { User } from '../../domain/model/entities/user.entities';
import { CommentResponse } from '../usecases/posts/update-post.usecase';

export type CreatePostRequest = {
    author: string;
    title: string;
    content: string;
};

export type CreatePostResponse = {
    id: string;
    title: string;
    content: string;
    author: string;
};

export type PostWithCommentsResponse = {
    id?: string;
    title?: string;
    content?: string;
    author?: string;
    comments?: CommentResponse[];
}

export type DeleteCommentRequest = {
    idPost: string;
    idComment: string;
    user: User;
}